package com.tcc.testeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(value = "com.tcc.testeapi.controller")
public class TesteApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteApiApplication.class, args);
	}

}
