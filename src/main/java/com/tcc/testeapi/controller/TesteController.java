package com.tcc.testeapi.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/teste")
public class TesteController {

    @GetMapping
    public ResponseEntity<String> getMensagem(){
        return ResponseEntity.status(HttpStatus.OK).body("teste");

    }
}
